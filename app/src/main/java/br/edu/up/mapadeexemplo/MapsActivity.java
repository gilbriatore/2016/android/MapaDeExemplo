package br.edu.up.mapadeexemplo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity
    implements OnMapReadyCallback, LocationListener,
    GoogleApiClient.ConnectionCallbacks {

  private GoogleMap mMap;
  private Marker marcadorAtual;
  private final int PERMISSAO_DE_RASTREAMENTO = 1;
  private LocationRequest locationRequest;
  private GoogleApiClient googleApiClient;
  private final float PERMITIDO = PackageManager.PERMISSION_GRANTED;
  private final String[] permissoes = {
      Manifest.permission.ACCESS_FINE_LOCATION,
      Manifest.permission.ACCESS_COARSE_LOCATION
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps);

    //Solicita permissões;
    if (isAPI23ouSuperior()){
      getPermissoes();
    }

    //Cria o cliente;
    if (googleApiClient == null) {
      googleApiClient = new GoogleApiClient.Builder(this)
          .addConnectionCallbacks(this)
          .addApi(LocationServices.API)
          .build();
    }

    //Notifica o fragmento quando o mapa estiver pronto para uso.
    FragmentManager fm = getSupportFragmentManager();
    SupportMapFragment smf = (SupportMapFragment) fm.findFragmentById(R.id.map);
    smf.getMapAsync(this);
  }

  @Override
  protected void onStart() {
    super.onStart();
    googleApiClient.connect();
  }

  @Override
  protected void onStop() {
    super.onStop();
    if (googleApiClient.isConnected()) {
      googleApiClient.disconnect();
    }
  }

  private boolean isPermitido(String permissao) {
    return ActivityCompat.checkSelfPermission(this, permissao) == PERMITIDO;
  }

  private boolean isAPI23ouSuperior() {
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      return true;
    }
    return false;
  }

  private boolean getPermissoes() {

    //Se não foi permitido ainda, solicita permissão;
    if (!isPermitido(permissoes[0]) || !isPermitido(permissoes[1])) {

      //Verifica se o usuário precisa de informações;
      if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissoes[0]) ||
          ActivityCompat.shouldShowRequestPermissionRationale(this, permissoes[1])) {
        ActivityCompat.requestPermissions(this, permissoes, PERMISSAO_DE_RASTREAMENTO);
      } else {
        ActivityCompat.requestPermissions(this, permissoes, PERMISSAO_DE_RASTREAMENTO);
      }
      return  false;
    } else {
      return true;
    }
  }

  @Override
  @SuppressWarnings("MissingPermission")
  public void onRequestPermissionsResult(
      int requestCode, String permissions[], int[] grant) {
    switch (requestCode) {
      case PERMISSAO_DE_RASTREAMENTO : {
        //Se permitido ativa rastreamento;
        if (grant.length > 0 && grant[0] == PERMITIDO) {
          mMap.setMyLocationEnabled(true);
        } else {
          Toast.makeText(this, "Rastreamento negado!", Toast.LENGTH_LONG).show();
        }
        return;
      }
    }
  }

  private void atualizarMapa(double latitude, double longitude){

    float zoom = mMap.getCameraPosition().zoom;
    LatLng latLng = new LatLng(latitude, longitude);

    //Cria o marcador;
    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bike);
    MarkerOptions marcador = new MarkerOptions();
    marcador.icon(BitmapDescriptorFactory.fromBitmap(bmp));
    marcador.position(latLng);
    marcadorAtual = mMap.addMarker(marcador);

    //Define a posição atual, aplica zoom e inclinação;
    CameraPosition position = CameraPosition.builder()
        .target(latLng)
        .zoom(zoom)
        .tilt(45)
        .build();

    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
  }

  @Override
  @SuppressWarnings("MissingPermission")
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    mMap.getUiSettings().setZoomControlsEnabled(true);
    mMap.getUiSettings().setCompassEnabled(true);
    mMap.getUiSettings().setMapToolbarEnabled(true);

    //Se permitido ativa rastreamento;
    if (isPermitido(permissoes[0]) && isPermitido(permissoes[1])){
      mMap.setMyLocationEnabled(true);
    }

    //Coordenadas da Universidade Positivo;
    atualizarMapa(-25.446828, -49.358875);
  }

  @Override
  public void onLocationChanged(Location location) {
    if (marcadorAtual != null) {
      marcadorAtual.remove();
    }
    atualizarMapa(location.getLatitude(), location.getLongitude());
  }

  @SuppressWarnings("MissingPermission")
  private void iniciarEscutaDeAtualizacao() {
    locationRequest = new LocationRequest();
    locationRequest.setInterval(1000);
    locationRequest.setFastestInterval(1000);
    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
  }

  @SuppressWarnings("MissingPermission")
  private void pararEscutaDeAtualizacao() {
    LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {
    if (isPermitido(permissoes[0]) && isPermitido(permissoes[1])) {
      iniciarEscutaDeAtualizacao();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (googleApiClient.isConnected()){
      pararEscutaDeAtualizacao();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (googleApiClient.isConnected()){
      iniciarEscutaDeAtualizacao();
    }
  }

  @Override
  public void onConnectionSuspended(int i) {
    //Não utilizado;
  }
}